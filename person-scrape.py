from bs4 import BeautifulSoup
import unicodecsv as csv
import sys
import time
import re

personId = str('108200')
# print("working on "+str(sys.argv[j]))
html = open('949051.html').read()
soup = BeautifulSoup(html, "lxml")
infoParent = soup.find_all("div", {"class": "col-md-8 col-xs-12 edit-list-section"})[0]
# age and gender . ile ayrilacak farkli datalar

personName = soup.find_all("h1", {"class":"edit-list-title"})[0].text.strip()

# ilk blok
info1 = infoParent.contents[1].contents[3]
personDataMain = info1.find_all("div",{"class":"edit-list-box edit-third"})[0].contents[1].contents[1]
personDataQ = []
personDataA = []
personQAData = personDataMain.find_all("div",{"class":"col-xs-12"})
for i in range(len(personQAData)):
    if(i%3==0):
        personDataQ.append(personQAData[i].contents[1].text.strip())
        personDataA.append(personQAData[i].contents[3])

for i in range(len(personDataA)):
    listFind = personDataA[i].find_all("li") 
    if(len(listFind)!=0):
        tempArr = []
        for j in range(len(listFind)):
            tempArr.append(listFind[j].text.strip())
        tempStr = '["'+'", "'.join(tempArr)+'"]'
        personDataA[i] = tempStr
    else:
        personDataA[i] = personDataA[i].text.strip()

# about this person dahil alttaki 4 blok
info2 = infoParent.find_all("div",{"class":"edit-list-box edit-fourth"})
aboutPersonQ = []
aboutPersonA = []
for i in range(len(info2)):
    aboutPersonQ.append(info2[i].find_all("div",{"class":"col-xs-10 lower-title"})[0].text.strip())
    aboutPersonA.append(info2[i].find_all("div",{"class":"col-xs-12 col-sm-12 edit-fo-details"})[0].text.strip())



info3 = infoParent.find_all("div",{"class":"edit-list-box edit-sixth"})

# home preference
homeDataMain = info3[0].find_all("div",{"class":"float-left width100 lower-border"})
homeDataQ = []
homeDataA = []
for i in range(len(homeDataMain)):
    # homeDataQ.append(homeDataMain[i].find_all("div",{"class":"col-md-4 col-sm-4 col-xs-12 paddingleft-10 lower-title"})[0].text.strip())
    # homeDataA.append(homeDataMain[i].find_all("div",{"class":"col-md-8 col-sm-8 col-xs-12 lower-details color-dot"})[0])
    homeDataQ.append(homeDataMain[i].contents[1].text.strip())
    homeDataA.append(homeDataMain[i].contents[3])
for i in range(len(homeDataA)):
    listFind = homeDataA[i].find_all("li") 
    if(len(listFind)!=0):
        tempArr = []
        for j in range(len(listFind)):
            tempArr.append(listFind[j].text.strip())
        tempStr = '["'+'", "'.join(tempArr)+'"]'
        homeDataA[i] = tempStr
    else:
        homeDataA[i] = homeDataA[i].text.strip()

# flatmate preferences
flatMain = info3[1].find_all("div",{"class":"float-left width100 lower-border"})
flatQ = []
flatA = []
for i in range(len(flatMain)):
    # flatQ.append(flatMain[i].find_all("div",{"class":"col-md-4 col-sm-4 col-xs-12 paddingleft-10 lower-title"})[0].text.strip())
    # flatA.append(flatMain[i].find_all("div",{"class":"col-md-8 col-sm-8 col-xs-12 lower-details color-dot"})[0])
    flatQ.append(flatMain[i].contents[1].text.strip())
    flatA.append(flatMain[i].contents[3])
for i in range(len(flatA)):
    listFind = flatA[i].find_all("li") 
    if(len(listFind)!=0):
        tempArr = []
        for j in range(len(listFind)):
            tempArr.append(listFind[j].text.strip())
        tempStr = '["'+'", "'.join(tempArr)+'"]'
        flatA[i] = tempStr
    else:
        flatA[i] = flatA[i].text.strip()


# location preferences
locMain = infoParent.find_all("div",{"class":"edit-list-box edit-nine"})[0]
locQ = "Location Preferences"
locA = []
listFind = locMain.find_all("li") 
if(len(listFind)!=0):
    tempArr = []
    for j in range(len(listFind)):
        tempArr.append(listFind[j].text.strip())
    tempStr = '["'+'", "'.join(tempArr)+'"]'
    locA = tempStr
else:
    locA = "No answer"

personDataQ = '['+', '.join(personDataQ)+']'
personDataA = '['+', '.join(personDataA)+']'
aboutPersonQ = '['+', '.join(aboutPersonQ)+']'
aboutPersonA = '['+', '.join(aboutPersonA)+']'
homeDataQ = '['+', '.join(homeDataQ)+']'
homeDataA = '['+', '.join(homeDataA)+']'
flatQ = '['+', '.join(flatQ)+']'
flatA = '['+', '.join(flatA)+']'
# locA = '["'+'", "'.join(locA)+'"]'

with open('maindata.csv', mode='a') as f:
    writer = csv.writer(f, encoding='utf-8')
    # writer.writerow(["id","person name",personDataQ,homeDataQ,flatQ,locQ])
    writer.writerow([personId,personName,personDataA,homeDataA,flatA,locA])
